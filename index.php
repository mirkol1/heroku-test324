<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    

<?php
//echo 'test'
//$pdo = new PDO('sqlite:database.sqlite');

if (!isset($_POST['submit'])) { 

?>

<form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="post">
<label for="name">Your name:</label>
<input type="text" name="name" required>
<label for="email">Add Email adress</label>
<input type="text" name="email" required>
<input type="submit" name="submit">
</form>

<?php 

} else 
{
    try
    {
        $db = new PDO('sqlite:emails.sqlite');
        $db->execute("CREATE TABLE email(name TEXT PRIMARY KEY, email TEXT)");
        $sql = "INSERT INTO emails (name, email) VALUES (:name, :email)";
        $stmt = $db->prepare($sql);

        $name = filter_input(INPUT_POST, 'name');
        $stmt->bindValue(':name', $name, PDO::PARAM_STR);


        $email = filter_input(INPUT_POST, 'name');
        $stmt->bindValue(':email', $email, PDO::PARAM_STR);


        $success = $stmt->execute();

        if($success)
        {
            echo 'Email address added to the database';
        } else
        {
            echo 'Sorry, something went wrong';
        }
    $db = null;
    } 
    catch (PDOException $e) 
    {
        print 'We had an error: ' . $e->getMessage() . '<br>';
        die();
    }
    
} 

?>
</body>
</html>